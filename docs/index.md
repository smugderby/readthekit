
# Introduction



You're already an investigator. This is why you're here: you have curiosity, or skills, or a problem that needs solving. You need to gather information, or you've found information and you need to know how to use it.

This kit was developed collectively by a group of activists, researchers, journalists, developers, ... , who have shared their knowledge on how to conduct investigations using a wide range of skills and techniques. But apart from that, the main thing that connects them, and what they essentially share with any reader of this material, is their curiosity about a certain problem, and a motivation to find more about it and act on it. So if you are reading this, you are already an investigator. An investigator in the making, at least, and here you will learn about different ways in which you can use that curiosity about an issue that motivates you to build a body of knowledge that helps you and others to better understand it. This is a process that essentially involves applying certain methods to gather, analyze and interpret information to shed light on the issue that you are addressing.

In a way, investigating is like putting together a puzzle. The pieces are scattered around, several of them are just lost. But you still have a drive to find out what the picture is, and you don't need all the pieces for that. Although challenging, there are different strategies that can be used to solve it, and this is what we want to show you here: how to identify and collect the pieces, how to put them together and how to figure out what meaning can be drawn from that incomplete picture.

If you are already familiar with this process and have already developed certain skills to do it, this material is also for you, and this resource will hopefully help to widen your horizons on new ways to investigate, new sources of information and data, new tools on how to gather, process, verify and analyze that data, and new ways on how to use it to meaningfully.

And if you already have a certain set of skills, but have not yet applied them for investigative purposes, don't worry, because here you will also see a number of real stories, about people just like you, who have gone through this {>>think of an adjective<<} transition from just being curious people who like to play with puzzles, to skillful investigators bringing light to important issues. Hopefully that will inspire you to find that curiosity in you and go on that same transition.

## What is an investigation?

An investigation is a process that involves the collection of information, and data from different sources. [AK - I'd like one sentence that gives the full overview right at the top here - so collection, but also analysis, drawing conclusions (I know you get to that, but I'd love to just have the one sentence definition)] This allows you to build a body of knowledge about a problem, a person, a landscape, a company, a political situation, a crime, power relations;[I'd rather put here specific problematiques, i.e. conflict between a local community and a mining project; money laundering; corruption; political strife...] you get the idea. Investigations are not just that, though. They also serve a purpose: you are answering a question, a group of questions, seeking truth or understanding, creating - or countering - a narrative. And the key to fulfilling that purpose is being able to turn that body of knowledge you have put together into a body of evidence, in that you interpret and attribute meaning to that knowledge. The processes of setting out your purpose, gathering information and distilling meaning is then the central structure of any investigation.

What constitutes evidence? Think again about a puzzle: each link or connection creates meaning and brings you one step closer to having a better idea of the larger picture. As you connect more pieces the picture starts to come into focus. No matter how much information you are able to collect, you have something you didn't have at the start: ideas on how to answer your question, a framework to keep searching within, a definition of scope, a sketch of an image or map, a recreation of an event or even the starting line for an entirely new investigation - in case the puzzle turns out to be much larger than you initially anticipated. Identifying those links between the pieces of information that you have gathered will ultimately help to bring you further in finding the answer you seek, and the evidence to document it.

Investigations are not limited to traditional detective-style work, or to strict question-and-answer formats. It is often a non-linear, iterative process where each piece added to the puzzle may lead you to see your original question from a different perspective. It may require you to rethink your strategies for finding more puzzle pieces, when the path that you have taken reaches a dead end. It may require you to look at your evidence in different ways, and consider multiple ways to interpret it, and new lines of investigation to uncover new evidence that refines your understanding of the picture you are putting together.

## Who is an investigator?

In 1917 France, in the midst of World War One, Jane Catulle-Mendes received a letter informing her that her son, Primice Mendes, a soldier in the French army, had been killed in action at the age of 20. In her diary, Mme Catulle-Mendes describes her unwillingness to accept that Primice was dead without any physical evidence. She decided she needed to know for herself: she had to find Primice's grave. With the assistance of one of Primice's comrades, Etienne D., Mme Catulle-Mendes learned that her son had been buried in the Black Forest in Germany, close to the front lines of the war. A white wooden cross identified his final resting place. She knew that such wartime cemeteries were often destroyed by bombardment, and that she had a limited opportunity to find his body and confirm what was reported to her. Mme Catulle-Mendes believed that "death had left her a task"; she couldn't allow her son to be unearthed by shellfire and his remains lost forever. {{>>@offray: I really like this historical introduction. I think that it shows promptly the human nature of investigation and how anyone can become into an investigator. In fact it remembers me of the movement from the "Madres de los Falsos positivos" in Colombia, where a group of courage women are trying to sheed light over the systematic dissapearence and murdering, by Colombian state forces, of young citizens that where abducted, killed and disguised as Guerrilleros. I don't think such woman or the people how knows their cause think originally of them as investigators, but after reading this paragraph is difficult not to do it.<<}

Mme Catulle-Mendes did indeed find her son's grave eventually, repatriated to Mourmelon-Le-Grand in France, roughly 400 km from his original grave in Germany [AK - so by the time she found the body it had already been moved to France, or was buried there in the first place?]. Three times, she attempted with the assistance of French soldiers to disinter his body under the cover of darkness, which protected them from continued bombardments at the advancing front. Finally, under the cover of night, Primice's casket was finally dug up; the night of October 6, 1917, Jane Catulle-Mendes was reunited with her son. She was lucky: the limestone ground of the cemetery had preserved Primice's body, and his face and identity were preserved as well.

Jane Catulle-Mendes was an investigator. Motivated by a desire to know for herself, she developed a body of knowledge about her son's comrades, whereabouts and final location until she was able to access the physical evidence she sought to confirm that the report of Primice's death was true. Without any technology, training or guidance, Mme Catulle-Mendes, a grieving mother, transformed herself into an investigator.

It doesn't matter if you work as an investigator, if you have training, experience, technology or skills. your job is defined as an investigator. The title is not as relevant as one might think. You may become an investigator by chance, even without claiming the title. Broadly, an investigator is someone who is motivated by a desire to know for themselves, who is animated by curiosity, who wants to apply research skills, technical expertise or simple motivation to solving a problem or answering a question by gathering and analyzing data. An investigator could be an amateur cartographer, a photographer, a trainspotter, an artist, a coder, a kid in his or her bedroom, an activist, or a grieving mother.

##  How can this kit help me?

I know what you're thinking. If I'm already an investigator and I already have all these skills and traits that already make me an investigator, why do I need to read all this? It's because investigators are continuously learning new techniques and exploring different ideas and tools in order to get answers to the questions they have. Investigation is not a linear process that can be mastered by simply following a step-by-step guide, but rather, it involves a mindset of approaching the question at hand from different angles, with various tools and techniques. The greater your arsenal of tools, methods, tactics and tips, the more you are able to grow and apply your investigative skills. The experiences of others, while inspiring to work on your own investigations, also offer an insight into the investigative mindset. You may want to explore sites on the internet that may not turn up in search engines, or websites that have been removed from the internet, but just don't know the simple steps of how to find them or access them. Some of the tools provided will help you access old websites, or extract data from videos and images that would otherwise remain invisible. In other words, they will help you recover those puzzle pieces that are not lost, but just really well hidden. Some of the techniques will give you ideas on how to approach data from unexpected points of entry. Even the most experienced investigators have a curiosity as to how others are doing their investigations and are continuously learning new techniques from others and various investigations they come across.

This kit hopes to amplify the investigator's mindset that is present in many of us out of curiosity or a desire to find out the truth or not [let ourselves be deceived] be lied to. Investigation is about a continuously evolving mindset that benefits from proven tools and tactics that can be used in conjunction to uncover facts. At a time of misinformation and saturation of data, it is important on a personal and a community level for us to be able to counter misleading narratives that are not based on facts and evidence. If facts are valuable, then the process of uncovering them is also valuable. Unfortunately this cannot be done by others for us all the time, sometimes we need to find out for ourselves and conduct our own investigations.
[AK - this second paragraph maybe belongs higher up in this document, framing what it is and why you would become an investigator. Or it's the last paragraph, the conclusion that leads people off into the tools and techniques and their investigation]

{>>@offray: I think that is important to underline the open source nature of this Kit in this section.
This converts it into a kind of meta-tool: a tool to build your own tools, to create your own adaptations, to share and recontextualize.
The work from its original authors is far from done, but is an starting point to create your own kit and the open source nature of this kit is
an enabler, in some way, for local communities and investigators to mold it for their needs and share their adaptations back to the broader investigators community<<}

## Curiosity killed the cat

Curiosity killed the cat. But perhaps the best answer to that famous saying is a quote attributed to Arnold Edinborough:

“Curiosity is the very basis of education and if you tell me that curiosity killed the cat, I say only the cat died nobly.”

While curiosity seems to be the basis of an investigator, it is still important to make sure you are aware of the dangers that may surround some of the investigations you pick. As you start investigating you really don't know what's cooking, so caution may need to be used. Before diving into passionately asking questions and looking up information and questioning individuals or entities that you think may be 'sources' of information, be mindful of your own safety and the risks that are involved in whatever tools and techniques you may want to use. Just as traces can be found to events or individuals that you're investigating, your own investigation may leave traces that may be undesirable which can put you at risk. There are plenty of things that you may need to do in order to secure yourself and those you're contacting or collaborating with. It's important to consider the environment you're in, whether it is a less technology driven environment, a restrictive environment or simply the internet, you may want to assess the risks you take by following some of the methods and techniques outlined in this kit.

# ABOUT THIS KIT

### BACKGROUND

why and how this kit was conceived, the work and thinking behind it
connections with ETI's history and the changing scene of investigative work around the world (unless this duplicates with the above but it should have different aproach

### FOCUS
how and why we decided to focus this kit on evidence collection
### AUDIENCE

who does this kit address
### CONTENTS
what modules we included here and why these are relevant to start with

how are we are going about the kit: staged releases etc.



## CONCEPTS AND PROCESSES

### INVESTIGATION

What is it, what counts as an investigation
Why do you investigate,what makes you start and what made other people start
How do you investigate
what it needs as 'ingredients'
what is the process, trying not to formalize it at all and leave room for creative work but still show that there is a process even if that is shaped and reshaped along the way based on lots of factors and findings (or lack of findings)
how you document your process and findings

Sources: https://void.tacticaltech.org/p/investigationkit_designing_your_investigation
     https://void.tacticaltech.org/p/investigationkit_chainofcustody_lawstuff
 https://void.tacticaltech.org/p/investigationkit_findinginformation
https://void.tacticaltech.org/p/investigationkit_analyzing+interpreting
https://void.tacticaltech.org/p/investigationkit_cleaningandmanagingdata


### EVIDENCE

(These below can be sub-sections under Evidence or some can go separately or in a different arrangement. I would also use a lot of the content from this pad here: https://void.tacticaltech.org/p/investigationkit_findinginformation

 https://void.tacticaltech.org/p/investigationkit_concepts

 What is Evidence
what is the difference between evidence, information, statements etc.
what is not evidence
how even lack of evidence constitutes evidence etc.
Sources of Evidence
Evidence you are given
Evidence that is open
Evidence you request
Evidence you provide (create?)
....
Formats/forms of Evidence
Image
Sound
Written
digital
analoque
....
Evidence Collection and Management
Safeguarding Evidence
Using Evidence

Sources: https://void.tacticaltech.org/p/investigationkit_verification, others


### DOCUMENTATION

documenting your investigation, method and entire process
referencing information and findings
preserving findings for later use and sharing
...


### VERIFICATION AND FACTCHECKING

what it means
why it matters
when it should happen
who should do it
how to do it - general approach
how to document your verification process (to provefindings)


### INVESTIGATION ENVIRONMENTS

 Digital research
 Desk resrach
 Field work
 Low-tech investigations

Sources: https://void.tacticaltech.org/p/investigationkit_lowtech


### INVESTIGATION TECHNIQUE

what they mean; how they define the data you find and collect
how they relate to your investigation questions and purpose
introduce the techniques we are focusing on here


### INVESTIGATION TYPE

what this means, what you need to know when you start, what questions you ask
how you identify and define your purpose, actors, necessary evidence and sources of information to begin with
introduce the types we are focusing on here


## HOW YOU INVESTIGATE

### FIELD RESEARCH
Sources:  https://void.tacticaltech.org/p/investigationkit_fieldresearchprinciples


### INTERVIEWS

- 1. Conducting Interviews
- 2. Source Management
Sources: https://void.tacticaltech.org/p/investigationkit_sourcemanagement


### RE/SEARCHING INTERNET WEBSITES

- 1. Google Dorking
- 2. Internet Archives

Sources: ETI Website
https://void.tacticaltech.org/p/investigationkit_webinv


### INVESTIGATING ON SOCIAL MEDIA

- 1. Find sources and eyewitnesses
- 2. How to find out more about specific topics/subjects
- 3. Verify Social Media Content
- 4. How to save and organise the list of findings
- 5. How to save posts, photos and videos from social networks

Sources:https://void.tacticaltech.org/p/investigationkit_social_media


### INVESTIGATING WITH IMAGES



### INVESTIGATING WITH MAPS AND GEOLOCATION

- 1.Geo data
- 2.Map formats
- 3.Mapping tools
- 4.Identifying places from photos
- 5.Processing/converting/collating geo data
- 6.Identification of features in satellite imagery
- 7.Observing change/ space and time
Sources:  https://void.tacticaltech.org/p/investigationkit_mapping


### INVESTIGATING WITH SOUND

- 1.Sound Basics - what you need to know
- 2.Investigating with available sound resources - collecting evidence from existing recordings
- 3.Creating sound resources for investigation - recording, capturing evidence, documenting the process
- 4.Voice Matching?
- 5.Investigations based on Sound (cases - can be incorporated throiughout the text above where it applies)

Sources:  https://void.tacticaltech.org/p/investigationkit_sound


### VERIFICATION TECHNIQUES
...
Sources: https://void.tacticaltech.org/p/investigationkit_verification


## WHAT YOU INVESTIGATE

### COMPANIES
(incl, profiling companies and related people, mapping ownership trees, reading reports and financial statements, identifying red flags etc.)
Sources: https://void.tacticaltech.org/p/investigationkit_fininv / Updated structure: https://void.tacticaltech.org/p/investigationkit_corporate

### PUBLIC FUNDS
(incl. public contracting, large infrastructure projects, preferrential agreements, red flags etc.)
Sources: https://void.tacticaltech.org/p/investigationkit_fininv / Updated structure: https://void.tacticaltech.org/p/investigationkit_corporate

### CORPORATE - POLITICAL CONNECTIONS
(incl. lobbying, revolving door, state capture etc.)
Sources: https://void.tacticaltech.org/p/investigationkit_fininv / Updated structure: https://void.tacticaltech.org/p/investigationkit_corporate

### PUBLIC STATEMENTS
(incl. identifying statements, contexts of statements, verification of claims and data, mapping data sources, revealing manipulation of data etc.)
Sources:https://void.tacticaltech.org/p/investigationkit_social_media
https://void.tacticaltech.org/p/investigationkit_verification

### SUPPLY CHAINS
(incl. tracking product sources, production processes, actors, red flags etc.)
Sources: https://void.tacticaltech.org/p/investigationkit_supplychains

### ELECTION INFLUENCE
(incl. actors and mechanisms involved in election campaigns, methods of influence, verification of campaign adds, use of voters information for targeted campaigns, businesses-candidates connections, red flags etc. --- pending topic)
Sources: TTC Data and Elections



## GENERAL CONSIDERATIONS
(not sure yet if we need this separately or we personalize them for each separate technique and type of investigation. We may need a general one on Safety)

### ETHICS
Sources: https://void.tacticaltech.org/p/investigationkit_ethics


### SAFETY
Sources: https://void.tacticaltech.org/p/investigationkit_risk

### LEGAL

### COLLABORATION
Sources:  https://void.tacticaltech.org/p/investigationkit_collaboration



## YOU ARE ALREADY AN INVESTIGATOR - WHERE NEXT

An open ending to this kit series (not a boring conclusion):
Talking about how you can use your evidence and investigations
Providing an overview of what else is possible to achieve from here on


PARKED:

https://void.tacticaltech.org/p/investigationkit_impact
https://void.tacticaltech.org/p/invesigationkit_hria
https://void.tacticaltech.org/p/investigationkit_best_practices_documentation
https://void.tacticaltech.org/p/investigationkit_post-its2




## ABOUT RIGHTS OF USE AND CONTRIBUTORS
